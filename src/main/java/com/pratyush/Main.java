package com.pratyush;

import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
	
	public static void main (String args[]) {
		
		// Initializing an LRUCache of size 5
		LRUCache lruCache = new LRUCache(5);
		
		// Adding 5 values
		lruCache.put(1, 10);
		lruCache.put(2, 20);
		lruCache.put(3, 30);
		lruCache.put(4, 40);
		lruCache.put(5, 50);
		
		// Original List
		System.out.println(lruCache.toString());
		
		// Adding 2 more values to exceed capacity
		lruCache.put(6, 60);
		System.out.println(lruCache.toString());
		lruCache.put(7, 70);
		System.out.println(lruCache.toString());
		
		//key = 1 and key = 2 have been evicted
		System.out.println(lruCache.get(1));
		System.out.println(lruCache.get(2));
		
		// key = 3 still works
		System.out.println(lruCache.get(3));
	}
}

class LRUCache extends LinkedHashMap<Integer, Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int size;
	LinkedHashMap<Integer, Integer> linkedHashMap;
	
	public LRUCache(int size) {
		this.size = size;
		linkedHashMap = new LinkedHashMap<Integer, Integer>(size, 0.75f, true);
	}
	
	@Override
	protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
		return this.size() > size;
	}
	
	// works in O(1)
	public int get(int key) {
		return this.getOrDefault(key, -1);
	}
	
	// works in O(1)
	public void put(int key, int value) {
		this.put(Integer.valueOf(key), Integer.valueOf(value));
	}
}
