# Least Recently Used (LRU)
A Least Recently Used (LRU) Cache organizes items in order of use, allowing you to quickly identify which item hasn't been used for the longest amount of time.

## An LRU cache is implemented by using 
1. A doubly linked list and
2. A hash map.

## Time Space Complexity
* Space - O(n)
* Get least recently used item - O(1)
* Access item - O(1)